import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Result, Api } from './last-result-f1.interfase';
import { LastResultF1Service } from './last-result-f1.service';


@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Hi there!</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Hello, {{name}}!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() name: any;

  constructor(public activeModal: NgbActiveModal) {}
}


@Component({
  selector: 'app-last-result-f1',
  templateUrl: './last-result-f1.component.html',
  styleUrls: ['./last-result-f1.component.css']
})
export class LastResultF1Component implements OnInit {

  constructor(private api:LastResultF1Service,private modalService: NgbModal) { }

  results: Array<Result> = [];
  raceName = '';

  ngOnInit(): void {  
    this.api.get().subscribe((data: Api) => {
      const selectRace = data.MRData.RaceTable.Races[0];
      this.results = selectRace.Results;
      this.raceName = selectRace.raceName;
      console.log(this.results);
      console.log(this.raceName);
    });

  }

  onclick(){
    alert('desde componente')
  }

  open() {
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.name = 'DESDE EL COMPONENTE';
  }
}
