import { NgModule , Injector} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { AppComponent } from './app.component';
import { LastResultF1Component } from './last-result-f1/last-result-f1.component';
import { LastResultF2Component } from './last-result-f2/last-result-f2.component';
import  {createCustomElement} from '@angular/elements';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

@NgModule({
  declarations: [
    AppComponent,
    LastResultF1Component,
    LastResultF2Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent, LastResultF1Component, LastResultF2Component]
})
export class AppModule {
  // Para convertir en web component
  constructor(private injector:Injector){
    const elementCustom = createCustomElement(
      LastResultF1Component,
      {
        injector:this.injector
      }
    );
    const elemento2 = createCustomElement(
      LastResultF2Component,
      {
        injector:this.injector
      }
    );
 
    const appComponent = createCustomElement(
      AppComponent,{
        injector: this.injector
      }
    );

    customElements.define('componente-1', elementCustom);
    customElements.define('componente-2', elemento2);
    customElements.define('app-componenete', appComponent);
  }
  ngDoBootstrap(): void{}

 }
